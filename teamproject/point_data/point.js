{
"stu1":{
        "name":"Alice",
        "ID":"001",
        "subject":{
            "english":50,
            "japanese":30,
            "math":45,
            "science":66,
            "society":85,
            "sum":276,
            "min_sub":"japanese"
        }
    },
    "stu2":{
        "name":"Bob",
        "ID":"010",
        "subject":{
        "english":62,
        "japanese":37,
        "math":84,
        "science":82,
        "society":23,
        "sum":288,
        "min_sub":"society"
    }
},
    "stu3":{
        "name":"Carol",
        "ID":"011",
        "subject":{
        "english":25,
        "japanese":33,
        "math":16,
        "science":84,
        "society":67,
        "sum":225,
        "min_sub":"math"
        }
    },
    "stu4":{
        "name":"Ellen",
        "ID":"100",
        "subject":{
        "english":27,
        "japanese":17,
        "math":53,
        "science":83,
        "society":35,
        "sum":215,
        "min_sub":"japanese"
    }
    },
    "stu5":{
        "name":"Frank",
        "ID":"101",
        "subject":{
        "english":37,
        "japanese":87,
        "math":97,
        "science":37,
        "society":48,
        "sum":306,
        "min_sub":"society"
    }
}
}