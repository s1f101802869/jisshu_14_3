from mainpage.models import Student
from mainpage.models import advice as ad
from mainpage.models import que
from mainpage.models import answer
from mainpage.models import kadai
from mainpage.models import notice
from mainpage.models import Time
from mainpage.models import Grades
from .form import StudentForm
from .form import KadaiForm
from .form import TimeForm
from .form import noticeForm
from .form import adviceForm
from .form import LoginForm
from .form import LoginForm2
from .form import queForm
from .form import ansForm
from .form import GradesForm
from django.views.generic import TemplateView
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.views.generic import CreateView
from mainpage.command.lib import point


def top(request):
    return render(request, 'top.html')
    
def studentform(request):
    if request.method == 'POST':
        form = StudentForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            #input_userid = form.cleaned_data('student_id')
            #input_username = form.cleaned_data('student_name')
            #input_password = form.cleaned_data('password1')
            #new_user = authenticate(student_id=input_userid, password=input_password) 
            if new_user is not None:
                login(request, new_user)
                return redirect('top')
    else:
        form = StudentForm()
    return render(request, 'kyouin/User registration.html', {'form': form})

def teacherform(request):
    if request.method == 'POST':
        form = StudentForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            new_user.is_staff = True
            new_user.save()
            #input_userid = form.cleaned_data('student_id')
            #input_username = form.cleaned_data('student_name')
            #input_password = form.cleaned_data('password1')
            #new_user = authenticate(student_id=input_userid, password=input_password) 
            if new_user is not None:
                login(request, new_user)
                return redirect('main')
    else:
        form = StudentForm()
    return render(request, 'kyouin/teacher_registration.html', {'form': form})

class Account_login(View):
    def post(self, request, *arg, **kwargs):
        form = LoginForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            user = Student.objects.get(student_id=username)
            login(request, user)
            if user.is_staff == True:
                return redirect('main')
            return redirect('top')
        return render(request, 'kyouin/Login.html', {'form': form,})

    def get(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        return render(request, 'kyouin/Login.html', {'form': form,})

account_login = Account_login.as_view()

def studentlist(request):
    posts = Student.objects.all()
    return render(request, 'student1.html', {'posts': posts})

def advicelist(request):
    if request.method == 'POST':
        form = queForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('./')
    else:
        form = queForm()
    advices = ad.objects.all()
    return render(request, 'advice.html', {'advices': advices, 'form': form})

def studentdetail(request, pk):
    post = Student.objects.get(pk=pk)
    return render(request, 'student2.html', {'post': post})

def kadaiformfunc(request):
    if request.method == 'POST':
        if 'kadaibutton' in request.POST:
            kadaiform = KadaiForm(request.POST)
            if kadaiform.is_valid():
                post = kadaiform.save(commit=False)
                post.save()
            noticeform = noticeForm()
            adviceform = adviceForm()
            return redirect('./')
        elif 'noticebutton' in request.POST:
            noticeform = noticeForm(request.POST)
            if noticeform.is_valid():
                post = noticeform.save(commit=False)
                post.save() 
            kadaiform = KadaiForm()
            adviceform = adviceForm()
            return redirect('./')
        elif 'advicebutton' in request.POST:
            adviceform = adviceForm(request.POST)
            if adviceform.is_valid():
                post = adviceform.save(commit=False)
                post.save() 
            kadaiform = KadaiForm()
            noticeform = noticeForm()
            return redirect('./')
    else:
        kadaiform = KadaiForm()
        noticeform = noticeForm()
        adviceform = adviceForm()
        return render(request, 'kyouin/main.html', {'kadaiform': kadaiform, 'noticeform': noticeform, 'adviceform': adviceform})
        
    
def kadailistfunc(request):
    posts = Post.objects.all()
    return render(request, 'kyouin/kyouin2-1.html', {'posts': posts})


def timeformfunc(request):
    if request.method == 'POST':
        if 'timebutton' in request.POST:
            time_form = TimeForm(request.POST)
            if time_form.is_valid():
                post = time_form.save(commit=False)
                user = request.user
                post.stu_id = user.student_id
                post.stu_name = user.student_name
                post.save()
                return redirect('detail', student_id=user.student_id)
            grade_form = GradesForm()
        elif 'gradesbutton' in request.POST:
            grade_form = GradesForm(request.POST)
            if grade_form.is_valid():
                post = grade_form.save(commit=False)
                user = request.user
                post.stu_id = user.student_id
                post.stu_name = user.student_name
                post.save()
                return redirect('stats')
            time_form = TimeForm()
    else:
        time_form = TimeForm()
        grade_form = GradesForm()
    return render(request, 'top.html', {'time_form': time_form, 'grade_form': grade_form})

def detail(request, student_id):
   user = get_object_or_404(Student, student_id=student_id)
   times = Time.objects.filter(stu_id = user.student_id)
   context = {
       'user' : user,
       'message': str(user.student_name) + 'さんの勉強時間',
       'times': times,
   }
   return render(request, 'detail.html', context)
#def account_login(request):
#    return render(request, 'login.html')

#@login_required
#def home(request):
#    return render(request, 'top.html')

class Index(View):
    @method_decorator(login_required)
    def get(self, request):
        return render(request, 'top.html')

def grade(request):
    times = Time.objects.all()
    t = point()
    data = {"point":t,"times":times}
    return render(request, 'kyouin/study.html', {"data":data})

def question(request):
    if request.method == 'POST':
        a_form = ansForm(request.POST)
        if a_form.is_valid():
            post = a_form.save(commit=False)
            post.save()
            return redirect('./')
    else:
        a_form = ansForm()
    questions = que.objects.all()
    return render(request, 'kyouin/Question.html', {'questions' : questions, 'a_form': a_form})

def stats(request):
    return render(request, 'stats.html')

def advice(request):
    return render(request, 'advice.html')


def queformfunc(request):
    if request.method == 'POST':
        form = queForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('./')
    else:
        form = queForm()
    return render(request, 'que.html', {'form': form})

def qafunc(request):
    if request.method == 'POST':
        form = queForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('./')
    else:
        form = queForm()
    questions = que.objects.all()
    answers = answer.objects.all()
    return render(request, 'publicQ.html', {'questions' : questions, 'answers' : answers, 'form': form})

def kadailistfunc(request):
    kadais = kadai.objects.all()
    notices = notice.objects.all()
    return render(request, 'work.html', {'kadais' : kadais, 'notices' : notices})

