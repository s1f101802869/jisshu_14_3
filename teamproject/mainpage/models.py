from django.db import models
from django.contrib.auth.models import AbstractBaseUser, _user_has_perm, BaseUserManager, PermissionsMixin

SUBJECT_CHOICES = (
    ('国語', '国語'),
    ('英語', '英語'),
    ('数学', '数学'),
    ('社会', '社会'),
    ('科学', '科学'),
)

# Create your models here.
class StudentManager(BaseUserManager):
    def create_user(self, student_name, student_id, password):
        if not student_id:
            raise ValueError('Users must have a id')
        if not student_name:
            raise ValueError('Users must have a name')
        
        #student_name = StudentManager.student_name(student_name)
        user = self.model(student_id=student_id, student_name=student_name, password=password)
        user.is_active = True
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, student_name, student_id, password):
        user = self.create_user(student_name=student_name, student_id=student_id, password=password)
        user.is_active = True
        user.is_staff = True
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user
#生徒
class Student(AbstractBaseUser, PermissionsMixin):
    student_name = models.CharField(max_length=50)
    student_id = models.IntegerField(null = False,default=0,unique=True)
    is_active   = models.BooleanField(default=True)
    is_staff    = models.BooleanField(default=False)
    is_admin    = models.BooleanField(default=False)
    USERNAME_FIELD = 'student_id'
    REQUIRED_FIELDS = ['student_name']
    #USERNAMEは一意でなくてはならないがstudent_nameは重なる可能性あり→idに変更
    objects = StudentManager()
    
    def user_has_perm(user, perm, obj):
        #permission系統のエラーの原因？
        return _user_has_perm(user, perm, obj)
    def has_perm(self, perm, obj=None):
        return _user_has_perm(self, perm, obj=obj)
    def has_module_perms(self, app_label):
        return self.is_admin
    @property
    def is_superuser(self):
        return self.is_admin
    
    class Meta:
        verbose_name = 'ユーザ'
        verbose_name_plural = 'ユーザ'
        db_table = 'student_user'
        swappable = 'AUTH_USER_MODEL'
#教員
class Teacher(models.Model):
    teacher_name = models.CharField(max_length=50)
    teacher_id = models.IntegerField(primary_key=True)
#テスト
class Exam(models.Model):
    subject = models.CharField(max_length=50)
    date = models.DateField()
    score = models.IntegerField()
#勉強時間
class Time(models.Model):
    subject = models.CharField(max_length=10, choices=SUBJECT_CHOICES, default='Japanese')
    date = models.DateField()
    amount = models.IntegerField()
    stu_id = models.IntegerField()
    stu_name = models.CharField(max_length=50)
#成績
class Grades(models.Model):
    subject = models.CharField(max_length=10, choices=SUBJECT_CHOICES, default='Japanese')
    date = models.DateField()
    score = models.IntegerField()
    stu_id = models.IntegerField()
    stu_name = models.CharField(max_length=50)
#課題
class kadai(models.Model):
    homework = models.CharField(max_length=200) 
    date = models.DateField()
    deadline = models.DateField()
    class_id = models.IntegerField(primary_key=True)
#お知らせ
class notice(models.Model):
    content = models.CharField(max_length=200)
#アドバイス
class advice(models.Model):
    ad_content = models.CharField(max_length=200)
#クラス
class classes(models.Model):
    date = models.DateField()
    deadline = models.DateField()
#質問
class que(models.Model):
    content = models.CharField(max_length=200) 
    note = models.CharField(max_length=200)

#返答
class answer(models.Model):
    que = models.CharField(max_length=200)
    content = models.CharField(max_length=200)

#Question.objects.create(title="タイトル１",question="質問１",question_id=1,created=datetime.strptime('2020-1-1','%Y-%m-%d'))
#Answer.objects.create(question_id=1,answer_id=1,answer="回答１",created=datetime.strptime('2020-1-1','%Y-%m-%d'))