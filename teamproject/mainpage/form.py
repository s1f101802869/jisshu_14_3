from django import forms
from .models import Student
from .models import kadai
from .models import Time
from .models import notice
from .models import advice
from .models import que
from .models import Grades
from .models import answer
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm

#class StudentForm(forms.ModelForm):
#    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
#    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)
#    class Meta:
#        model = Student
#        fields = ('student_name', 'student_id')
#    def clean_password2(self):
#        password1 = self.cleaned_data.get("password1")
#        password2 = self.cleaned_data.get("password2")
#        if password1 and password2 and password1 != password2:
#            raise forms.ValidationError("Passwords don't match")
#        return password2

#    def save(self, commit=True):
#        user = super().save(commit=False)
#        user.set_password(self.cleaned_data["password1"])
#        if commit:
#            user.save()
#        return user
#class StudentChangeForm(forms.ModelForm):
#    password = ReadOnlyPasswordHashField()

#    class Meta:
#        model = Student
#        fields = ('student_name', 'password', 'student_id',
#                  'is_active', 'is_admin')

#    def clean_password(self):
#        return self.initial["password"]

class StudentForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['student_id'].widget.attrs['class'] = 'form-control'
        self.fields['student_name'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'

    class Meta:
       model = Student
       fields = ("student_id", "student_name", "password1",  "password2",)

class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
       super().__init__(*args, **kwargs)
       self.fields['username'].widget.attrs['class'] = 'form-control'
       self.fields['password'].widget.attrs['class'] = 'form-control'
    
class LoginForm2(forms.Form):
    student_id = forms.IntegerField()
    password = forms.CharField()

class KadaiForm(forms.ModelForm):
    class Meta:
        model = kadai
        fields = ('homework', 'date', 'deadline', 'class_id')
        widgets = {
            'homework': forms.Textarea(attrs={'rows':4, 'cols':15,'placeholder':'課題を入力'}),
            'date' : forms.SelectDateWidget,
            'deadline' : forms.SelectDateWidget
        }
        labels = {
            'homework' : '課題',
            'date': '日付',
            'deadline': '締め切り',
            'class_id' : 'クラスid'
        }

class TimeForm(forms.ModelForm):
    class Meta:
        model = Time
        fields = ('date', 'amount', 'subject')
        labels = {
            'date': '日時',
            'amount': '勉強量(時間)',
            'subject' : '勉強科目'
        }
        widgets = {
            'date': forms.SelectDateWidget
        }

class adviceForm(forms.ModelForm):
    class Meta:
        model = advice
        fields = ('ad_content',)
        labels = {
            'ad_content' : 'アドバイス'
       }
        widgets = {
            'ad_content': forms.Textarea(attrs={'rows':4, 'cols':15,'placeholder':'アドバイスを入力'})
        }

class noticeForm(forms.ModelForm):
    class Meta:
        model = notice
        fields = ('content',)
        labels = {
            'content' : 'お知らせ'
       }
        widgets = {
            'content': forms.Textarea(attrs={'rows':4, 'cols':15,'placeholder':'お知らせを入力'})
        }

class queForm(forms.ModelForm):
    class Meta:
        model = que
        fields = ('content', 'note')
        labels = {
            'content' : '質問',
            'note' : '備考'
       }
        widgets = {
            'content': forms.Textarea(attrs={'rows':4, 'cols':15,'placeholder':'質問を入力'}),
            'note': forms.Textarea(attrs={'rows':4, 'cols':15,'placeholder':'備考を入力'})
        }

class ansForm(forms.ModelForm):
    class Meta:
        model = answer
        fields = ('que', 'content')
        labels = {
            'que' : '答える質問',
            'content' : '質問への回答'
       }
        widgets = {
            'que': forms.Textarea(attrs={'rows':4, 'cols':15,'placeholder':'答える質問'}),
            'content': forms.Textarea(attrs={'rows':4, 'cols':15,'placeholder':'回答を入力'})
        }

class GradesForm(forms.ModelForm):
    class Meta:
        model = Grades
        fields = ('date', 'score', 'subject')
        labels = {
            'date': '日時',
            'score': '点数',
            'subject' : '科目'
        }
        widgets = {
            'date': forms.SelectDateWidget
        }