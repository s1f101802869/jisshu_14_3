from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _
from .models import Student
from .form import StudentForm

# Register your models here.
#class StudentAdmin(admin.ModelAdmin):
#    model = Student
#    fields = ['student_name']

class StudentAdmin(UserAdmin):
    form = StudentForm
    list_display = ('student_id', 'student_name', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('student_id', 'password')}),
        ('Personal info', {'fields': ('student_name',)}),
        ('Permissions', {'fields': ('is_admin',)}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('student_id', 'student_name', 'password1', 'password2')}
         ),
    )
    search_fields = ('student_id',)
    ordering = ('student_id',)
    filter_horizontal = ()
    

admin.site.register(Student, StudentAdmin)
#admin.site.unregister(Group)