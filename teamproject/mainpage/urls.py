from django.urls import path,include
from . import views
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from .views import Index

urlpatterns = [
    path(r'top/',views.timeformfunc,name='top'),
    #path(r'kyouin1/',views.kyouin1,name="kyouin1"),
    path(r'main/',views.kadaiformfunc,name="main"),
    #path(r'kyouin3/',views.kyouin3,name="kyouin3"),
    #path(r'kyouin4/',views.kyouin4,name="kyouin4"),
    #ユーザー登録
    #path(r'', views.studentform, name='form'),
    #ログイン
#    path(r'', views.account_login, name='login'),
    path(r'study/',views.grade,name='study'),
    path(r'question/',views.question,name='question'),
    path('<int:student_id>', views.detail, name='detail'),
    path(r'stats/',views.stats,name='stats'),
    path(r'advice/',views.advice,name='advice'),
    path(r'que/',views.queformfunc,name='que'),
    path(r'QA/',views.qafunc,name='qa'),
    path(r'kadai/',views.kadailistfunc, name='kadai'),
    path(r'advicelist/',views.advicelist,name='advicelist'),
    path(r'detail/<int:pk>',views.studentdetail,name='detail'),
    path(r'regist/', views.studentform, name='regist'),
    path(r'T_regist/', views.teacherform, name='T_regist'),
    #path(r'login', views.authentication, name='Login'),
    url(r'login/', views.account_login, name='login'),
    #url(r'login/', auth_views.LoginView.as_view(template_name="kyouin/Login.html"), name='login'),
    path(r"logout/", auth_views.LogoutView.as_view(), name="logout"),
    url(r'', Index.as_view(), name='account_index'),
    ]


